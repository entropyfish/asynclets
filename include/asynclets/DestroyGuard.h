/*
The MIT License (MIT)

Copyright (c) 2014 Entropyfish

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// DestroyGuard prevents a callback to be called after the owner object has
// been destroyed. See README for more info.

#ifndef __DESTROY_GUARD_H__
#define __DESTROY_GUARD_H__

#include <memory>

namespace asynclets {

namespace detail {
  template<typename T>
  std::shared_ptr<T> deep_copy(std::shared_ptr<T> other) {
    return std::make_shared<T>(*other);
  }
}

class DestroyGuard {
public:
  class Indicator {
  public:

    explicit operator bool () const {
      return *_destroyed;
    }

  private:

    Indicator(std::shared_ptr<bool> destroyed)
      : _destroyed(destroyed)
    {}

  private:

    std::shared_ptr<bool> _destroyed;

    friend class DestroyGuard;
  };

  template<typename F>
  class GuardedFunction {
  public:

    template<typename... Args>
    void operator () (Args&&... args) {
      if (!*_destroyed) {
        _function(std::forward<Args>(args)...);
      }
    }

  private:

    template<typename G>
    GuardedFunction(G&& function, std::shared_ptr<bool> destroyed)
      : _function(std::forward<G>(function))
      , _destroyed(destroyed)
    {}

  private:
    F                     _function;
    std::shared_ptr<bool> _destroyed;

    friend class DestroyGuard;
  };


public:
  DestroyGuard() : _destroyed(std::make_shared<bool>(false)) {}

  ~DestroyGuard() {
    if (_destroyed) *_destroyed = true;
  }

  // Copies do not share the internal state.
  DestroyGuard(const DestroyGuard& other)
    : _destroyed(detail::deep_copy(other._destroyed))
  {}

  DestroyGuard& operator = (const DestroyGuard& other) {
    *_destroyed = true;
    _destroyed = detail::deep_copy(other._destroyed);
    return *this;
  }

  DestroyGuard(DestroyGuard&&) = default;

  DestroyGuard& operator = (DestroyGuard& other) {
    *_destroyed = true;
    _destroyed = std::move(other._destroyed);
    return *this;
  }

  Indicator make_indicator() const {
    return { _destroyed };
  }

  template<typename F>
  GuardedFunction<typename std::decay<F>::type> guard(F&& function) {
    return { std::forward<F>(function), _destroyed };
  }

  // Terse alternative to guard()
  template<typename F>
  auto operator () (F&& function) -> decltype(guard(std::forward<F>(function)))
  {
    return guard(std::forward<F>(function));
  }

private:

  std::shared_ptr<bool> _destroyed;
};

} // namespace asynclets

#endif // ifndef __DESTROY_GUARD_H__

