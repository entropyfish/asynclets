/*
The MIT License (MIT)

Copyright (c) 2014 Entropyfish

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef __WHEN_ALL_H__
#define __WHEN_ALL_H__

#include <functional>

namespace asynclets {

class WhenAll {
public:
  template<class Handler> WhenAll(const Handler& handler)
    : _instance_count(new size_t(0))
    , _handler(handler)
  {}

  std::function<void()> make_continuation() {
    if (!_handler) return [](){};

    auto handler = _handler;
    auto count   = _instance_count;

    ++*count;

    return [handler, count]() {
      if (--*count == 0) {
        handler();
      }
    };
  }

private:
  std::shared_ptr<size_t> _instance_count;
  std::function<void()>   _handler;
};

}

#endif // ifndef __WHEN_ALL_H__
