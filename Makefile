.PHONY: all test clean

COMPILER := g++
# COMPILER := clang

CFLAGS := -std=c++11 -Wall -Iinclude \
					-DBOOST_TEST_DYN_LINK 		 \
					-DBOOST_TEST_MODULE=main

LFLAGS := -lboost_unit_test_framework

COMPONENTS := DestroyGuard

TEST_DIR := tests
HEADER_DIR := include/asynclets

TESTS := $(addprefix $(TEST_DIR)/, $(addsuffix .test, $(COMPONENTS)))

all: test

test: $(TESTS)
	@for test in $(TESTS);	do ./$$test;	done

# $(TEST_DIR)/DestroyGuard.test: $(TEST_DIR)/DestroyGuard.test.cpp $(HEADER_DIR)/DestroyGuard.h
$(TEST_DIR)/%.test: $(TEST_DIR)/%.test.cpp $(HEADER_DIR)/%.h
	$(COMPILER) $(CFLAGS) -o $@ $< $(LFLAGS)

clean:
	rm -f $(TESTS)
