#include <memory>
#include <boost/test/unit_test.hpp>
#include "asynclets/DestroyGuard.h"

using namespace asynclets;

BOOST_AUTO_TEST_CASE(test_indicator_evaluates_to_false_if_destroy_guard_is_not_destroyed)
{
  DestroyGuard dg;
  auto destroyed = dg.make_indicator();

  BOOST_CHECK(!destroyed);
}

BOOST_AUTO_TEST_CASE(test_indicator_evaluates_to_true_if_destroy_guard_is_destroyed)
{
  DestroyGuard dg;
  auto destroyed = dg.make_indicator();

  dg = DestroyGuard();

  BOOST_CHECK(destroyed);
}

BOOST_AUTO_TEST_CASE(test_callback_is_called_if_destroy_guard_is_not_destroyed)
{
  DestroyGuard dg;
  bool called = false;

  auto original_callback = [&]() {
    called = true;
  };

  auto guarded_callback = dg.guard(original_callback);
  guarded_callback();

  BOOST_CHECK(called);
}

BOOST_AUTO_TEST_CASE(test_callback_is_not_called_if_destroy_guard_is_destroyed)
{
  DestroyGuard dg;
  bool called = false;

  auto original_callback = [&]() {
    called = true;
  };

  auto guarded_callback = dg.guard(original_callback);

  dg = DestroyGuard();

  guarded_callback();

  BOOST_CHECK(!called);
}

