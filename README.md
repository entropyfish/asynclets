Asynclets - Helpers for asynchronous programming in C++
=======================================================

Asynclets is a library of small, self-contained, reusable and composable
utilities to help with asynchronous programming in C++11/14.

Contents
--------

Currently contains these components:

- [DestroyGuard](#DestroyGuard) - Protects handlers from destruction of their
  owner object
- [WhenAll](#WhenAll) - Call a handler when all of a given set of asynchronous
  operations finish

Everything is defined in namespace `asynclets`.

### DestroyGuard

#### Include

```c++
#include <asynclets/DestroyGuard.h>
```

#### Description

DestroyGuard prevents asynchronous callbacks to access objects which have been
destroyed.

TODO: Usage, Examples, ...

### WhenAll

#### Include

```c++
#include <asynclets/WhenAll.h>
```

TODO: Description, Usage

Installation
------------

TODO

Authors
-------

- [Peter Jankuliak](https://bitbucket.org/inetic)
- [Adam Cigánek](https://bitbucket.org/adam_ciganek)

License
-------

The MIT License (MIT)

Copyright (c) 2014 Entropyfish

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.